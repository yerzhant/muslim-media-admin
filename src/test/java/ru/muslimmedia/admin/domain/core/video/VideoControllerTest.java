package ru.muslimmedia.admin.domain.core.video;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class VideoControllerTest {

    @Autowired
    private TestRestTemplate template;

    @Test
    public void increaseViews() {
        System.out.println(template.patchForObject("/videos/increase-views", "3434", Integer.class));
    }
}