package ru.muslimmedia.admin.domain.core.quotation;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.hateoas.config.EnableEntityLinks;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@EnableEntityLinks
public class QuotationRepositoryTest {

    @Autowired
    private QuotationRepository repository;

    @Test
    public void findTop1ByOrderByPublishedOnDesc() {
        System.out.println(repository.findTop1ByOrderByPublishedOnDesc());
    }
}