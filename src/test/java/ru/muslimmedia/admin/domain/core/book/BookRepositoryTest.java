package ru.muslimmedia.admin.domain.core.book;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BookRepositoryTest {

    @Autowired
    private TestRestTemplate template;

    @Test
    public void findBySectionOrderByPublishedOnDesc() {
        System.out.println(template.getForEntity("/api/books/search/findBySectionOrderByPublishedOnDesc", String.class));
    }
}