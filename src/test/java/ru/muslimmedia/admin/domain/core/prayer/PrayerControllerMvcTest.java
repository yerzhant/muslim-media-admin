package ru.muslimmedia.admin.domain.core.prayer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(PrayerController.class)
@DataJpaTest
public class PrayerControllerMvcTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void cities() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/prayer-times/cities"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(mvcResult -> System.out.println("XXX: " + mvcResult.getResponse().getContentAsString()));
    }

    @Test
    public void today() {
    }
}