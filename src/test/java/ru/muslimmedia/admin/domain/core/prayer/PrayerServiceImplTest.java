package ru.muslimmedia.admin.domain.core.prayer;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalTime;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PrayerServiceImplTest {

    @Autowired
    private PrayerService service;

    @Autowired
    private PrayerRepository repository;

    @MockBean
    private MultipartFile file;

    @Test
    public void processFile() throws IOException, InvalidFormatException {
        InputStream is = new BufferedInputStream(new FileInputStream("/home/yerzhan/tmp/namaz_vakytlary_0-cutted.xlsx"));
        BDDMockito.given(file.getInputStream()).willReturn(is);
        service.loadFile(file);
        Prayer prayer = repository.findByDateAndCity(LocalDate.of(2018, 1, 13), "Казань");
        Assertions.assertThat(prayer.getFajr()).isEqualTo(LocalTime.of(6, 35, 33));
    }
}