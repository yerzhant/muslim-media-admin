package ru.muslimmedia.admin.domain.core.prayer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.hateoas.config.EnableEntityLinks;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@EnableEntityLinks
public class PrayerRepositoryTest {

    @Autowired
    private PrayerRepository repository;

    @Test
    public void findDistinctCity() {
        List<PrayerRepository.City> cities = repository.findDistinctCityByOrderByCity();
        cities.forEach(System.out::println);
    }
}