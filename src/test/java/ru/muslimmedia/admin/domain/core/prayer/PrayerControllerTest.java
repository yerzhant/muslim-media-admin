package ru.muslimmedia.admin.domain.core.prayer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PrayerControllerTest {

    @Autowired
    private TestRestTemplate template;

    @Test
    public void today() {
        System.out.println(template.getForEntity("/prayer-times/today/Казань", Object.class));
    }

    @Test
    public void cities() {
        System.out.println(template.getForEntity("/prayer-times/cities", Object.class));
    }
}