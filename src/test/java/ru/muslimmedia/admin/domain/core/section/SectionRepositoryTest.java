package ru.muslimmedia.admin.domain.core.section;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.hateoas.EntityLinks;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class SectionRepositoryTest {

    @Autowired
    private SectionRepository repository;

    @MockBean
    private EntityLinks entityLinks;

    @Test
    public void findDistinctCity() {
        Iterable<Section> all = repository.findAll();
        all.forEach(System.out::println);
    }
}