package ru.muslimmedia.admin.domain.core.prayer;

import org.junit.Test;

import java.time.Instant;
import java.time.ZoneId;
import java.time.chrono.HijrahDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class PrayerServiceImplDateTest {

    @Test
    public void processFile() {
        Date date = new Date();
        Instant instant = date.toInstant();
        System.out.println(instant.atZone(ZoneId.systemDefault()).toLocalTime());
    }

    @Test
    public void hidjraDate() {
        HijrahDate hijrahDate = HijrahDate.now().plus(0, ChronoUnit.DAYS);
        System.out.println(DateTimeFormatter.ofPattern("d MMMM yyyy").format(hijrahDate));
    }

}