package ru.muslimmedia.admin.domain.integration.filemanager;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.hateoas.EntityLinks;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class StorageServiceImplTest {

    @MockBean
    private MultipartFile file;

    @MockBean
    private EntityLinks entityLinks;

    @Autowired
    private StorageService service;

    @Test
    public void simple() {
        Path dir = Paths.get("aaa");
        System.out.println(dir.resolve("subDir").resolve("fff"));
    }

    @Test
    public void store() throws IOException {
        try (InputStream is = new BufferedInputStream(new FileInputStream("/home/yerzhan/tmp/namaz_vakytlary_0-cutted.xlsx"))) {
            BDDMockito.given(file.getInputStream()).willReturn(is);
            BDDMockito.given(file.getOriginalFilename()).willReturn("abc.xlsx");
            service.store(file, "media");
        }
    }

    @Test
    public void delete() {
    }
}