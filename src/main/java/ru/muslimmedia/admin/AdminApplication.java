package ru.muslimmedia.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import ru.muslimmedia.admin.domain.core.book.Book;
import ru.muslimmedia.admin.domain.core.post.Post;
import ru.muslimmedia.admin.domain.core.setting.Setting;
import ru.muslimmedia.admin.domain.core.setting.SettingRepository;
import ru.muslimmedia.admin.domain.core.video.Video;

import java.util.Arrays;

@SpringBootApplication
public class AdminApplication implements CommandLineRunner {

    @Autowired
    private SettingRepository settingRepository;

    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class, args);
    }

    @Override
    public void run(String... strings) {
        Arrays.stream(Setting.Type.values()).forEach(type -> {
            Setting setting = settingRepository.findByType(type);
            if (setting == null) {
                settingRepository.save(new Setting(type, " "));
            }
        });
    }

    @Bean
    public BookProcessor bookProcessor(EntityLinks entityLinks) {
        return new BookProcessor(entityLinks);
    }

    @Bean
    public PostProcessor postProcessor(EntityLinks entityLinks) {
        return new PostProcessor(entityLinks);
    }

    @Bean
    public VideoProcessor videoProcessor(EntityLinks entityLinks) {
        return new VideoProcessor(entityLinks);
    }

    private static class BookProcessor implements ResourceProcessor<Resource<Book>> {

        private final EntityLinks entityLinks;

        private BookProcessor(EntityLinks entityLinks) {
            this.entityLinks = entityLinks;
        }

        @Override
        public Resource<Book> process(Resource<Book> resource) {
            Link link = entityLinks.linkToSingleResource(resource.getContent().getSection());
            resource.add(link.withRel("sectionRef"));
            return resource;
        }
    }

    private static class PostProcessor implements ResourceProcessor<Resource<Post>> {

        private final EntityLinks entityLinks;

        private PostProcessor(EntityLinks entityLinks) {
            this.entityLinks = entityLinks;
        }

        @Override
        public Resource<Post> process(Resource<Post> resource) {
            Link link = entityLinks.linkToSingleResource(resource.getContent().getSection());
            resource.add(link.withRel("sectionRef"));
            return resource;
        }
    }

    private static class VideoProcessor implements ResourceProcessor<Resource<Video>> {

        private final EntityLinks entityLinks;

        private VideoProcessor(EntityLinks entityLinks) {
            this.entityLinks = entityLinks;
        }

        @Override
        public Resource<Video> process(Resource<Video> resource) {
            Link teacherLink = entityLinks.linkToSingleResource(resource.getContent().getTeacher());
            Link sectionLink = entityLinks.linkToSingleResource(resource.getContent().getSection());
            resource.add(teacherLink.withRel("teacherRef"));
            resource.add(sectionLink.withRel("sectionRef"));
            return resource;
        }
    }
}
