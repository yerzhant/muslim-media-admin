package ru.muslimmedia.admin.domain.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    public static Long getIdFromUri(String uri, String prefix) {
        Pattern pattern = Pattern.compile(".+/" + prefix + "/(\\d+)$");
        Matcher matcher = pattern.matcher(uri);
        if (matcher.matches()) {
            return Long.valueOf(matcher.group(1));
        } else {
            throw new IllegalArgumentException("Can't get an id from the uri: " + uri);
        }
    }
}
