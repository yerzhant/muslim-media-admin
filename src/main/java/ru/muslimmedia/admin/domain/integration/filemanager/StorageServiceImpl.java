package ru.muslimmedia.admin.domain.integration.filemanager;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.*;

@Service
public class StorageServiceImpl implements StorageService {

    private static final String STORAGE_DIR_NAME = "storage";
    private static final Path STORAGE_DIR_PATH = Paths.get(STORAGE_DIR_NAME);

    @Override
    public void store(MultipartFile file) throws IOException {
        store(file, "");
    }

    @Override
    public void store(MultipartFile file, String subDir) throws IOException {
        String filename = file.getOriginalFilename();

        if (file.isEmpty()) {
            throw new StorageException("File is empty: " + filename);
        }

        Files.copy(file.getInputStream(), STORAGE_DIR_PATH.resolve(subDir).resolve(filename), StandardCopyOption.REPLACE_EXISTING);
    }

    @Override
    public void delete(String filename) {
        try {
            Files.delete(STORAGE_DIR_PATH.resolve(filename));
        } catch (IOException e) {
            throw new StorageFileNotFoundException("File not found: " + filename, e);
        }
    }
}
