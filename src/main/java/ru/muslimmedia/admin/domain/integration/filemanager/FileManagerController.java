package ru.muslimmedia.admin.domain.integration.filemanager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@BasePathAwareController
@RequestMapping("file-manager")
public class FileManagerController {

    private final StorageService storageService;

    @Autowired
    public FileManagerController(StorageService storageService) {
        this.storageService = storageService;
    }

    @PostMapping("upload")
    public void upload(@RequestParam("file") MultipartFile file) throws IOException {
        storageService.store(file);
    }

    @PostMapping("upload-media")
    public void uploadMedia(@RequestParam("file") MultipartFile file) throws IOException {
        storageService.store(file, "media");
    }

    @DeleteMapping("delete/{filename:.+}")
    public void delete(@PathVariable String filename) {
        storageService.delete(filename);
    }
}
