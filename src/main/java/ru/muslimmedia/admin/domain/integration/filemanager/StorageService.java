package ru.muslimmedia.admin.domain.integration.filemanager;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface StorageService {

    void store(MultipartFile file) throws IOException;

    void store(MultipartFile file, String subDir) throws IOException;

    void delete(String filename);
}
