package ru.muslimmedia.admin.domain.integration.filemanager;

class StorageFileNotFoundException extends StorageException {

    StorageFileNotFoundException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
