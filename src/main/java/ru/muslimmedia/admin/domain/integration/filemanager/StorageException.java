package ru.muslimmedia.admin.domain.integration.filemanager;

class StorageException extends RuntimeException {

    StorageException(String s) {
        super(s);
    }

    StorageException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
