package ru.muslimmedia.admin.domain.core.book;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import ru.muslimmedia.admin.domain.core.BaseEntity;
import ru.muslimmedia.admin.domain.core.section.Section;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Data
public class Book extends BaseEntity {

    @NotEmpty
    private String title;

    @NotEmpty
    private String url;

    @NotNull
    private LocalDate publishedOn;

    private String authors;

    @NotNull
    @ManyToOne
    private Section section;
}
