package ru.muslimmedia.admin.domain.core.quote;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

@SuppressWarnings("unused")
public interface QuoteRepository extends CrudRepository<Quote, Long> {

    List<Quote> findByOrderByWeight();
}
