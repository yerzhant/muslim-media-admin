package ru.muslimmedia.admin.domain.core.quotation;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

@SuppressWarnings("unused")
public interface QuotationRepository extends CrudRepository<Quotation, Long> {

    Quotation findTop1ByOrderByPublishedOnDesc();

    Page findByOrderByPublishedOnDesc(Pageable pageable);
}
