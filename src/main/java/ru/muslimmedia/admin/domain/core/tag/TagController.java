package ru.muslimmedia.admin.domain.core.tag;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.hateoas.EntityLinks;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@BasePathAwareController
@RequestMapping("tags")
public class TagController {

    private final TagRepository repository;

    private final EntityLinks entityLinks;

    @Autowired
    public TagController(TagRepository repository, EntityLinks entityLinks) {
        this.repository = repository;
        this.entityLinks = entityLinks;
    }

    @PostMapping("get-ids")
    public String[] getIds(@RequestBody TagsTitles tagsTitles) {

        List<String> ids = new ArrayList<>();

        Arrays.stream(tagsTitles.tags).forEach(t -> {

            Tag tag = repository.findByTitle(t);

            if (tag == null) {
                tag = repository.save(new Tag(t));
            }

            ids.add(entityLinks.linkForSingleResource(tag).toString());
        });

        return ids.toArray(tagsTitles.tags);
    }

    @Data
    private static class TagsTitles {

        private String[] tags;
    }
}
