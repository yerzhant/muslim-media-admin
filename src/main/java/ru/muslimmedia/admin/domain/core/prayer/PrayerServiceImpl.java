package ru.muslimmedia.admin.domain.core.prayer;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import ru.muslimmedia.admin.domain.core.setting.Setting;
import ru.muslimmedia.admin.domain.core.setting.SettingRepository;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.chrono.HijrahDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class PrayerServiceImpl implements PrayerService {

    private final PrayerRepository repository;

    private final SettingRepository settingRepository;

    public PrayerServiceImpl(PrayerRepository repository, SettingRepository settingRepository) {
        this.repository = repository;
        this.settingRepository = settingRepository;
    }

    @Override
    public void loadFile(MultipartFile file) throws IOException, InvalidFormatException {

        try(Workbook workbook = WorkbookFactory.create(file.getInputStream())) {
            Sheet sheet = workbook.getSheetAt(0);
            int numberOfRows = sheet.getPhysicalNumberOfRows();

            repository.deleteAll();

            for (int i = 2; i < numberOfRows; i++) {
                Row row = sheet.getRow(i);
                if (row == null) {
                    break;
                }

                String city = row.getCell(0).getStringCellValue();
                LocalDate date = row.getCell(1).getDateCellValue().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                LocalTime fajr = getTime(row, 3);
                LocalTime sunrise = getTime(row, 4);
                LocalTime dhuhr = getTime(row, 5);
                LocalTime asr = getTime(row, 7);
                LocalTime maghrib = getTime(row, 8);
                LocalTime isha = getTime(row, 9);

                repository.save(new Prayer(city, date, fajr, sunrise, dhuhr, asr, maghrib, isha));
            }
        }
    }

    @Override
    public Prayer getPrayerTimesForToday(String city) {
        return repository.findByDateAndCity(LocalDate.now(), city);
    }

    @Override
    public List<String> getAllCities() {
        List<PrayerRepository.City> cities = repository.findDistinctCityByOrderByCity();
        return cities.stream().map(PrayerRepository.City::getCity).collect(Collectors.toList());
    }

    @Override
    public String getHijrahDate() {
        String hijrahCorrection = settingRepository.findByType(Setting.Type.HIJRAH_CORRECTION).getValue();
        HijrahDate hijrahDate = HijrahDate.now().plus(Long.parseLong(hijrahCorrection), ChronoUnit.DAYS);
        return DateTimeFormatter.ofPattern("d MMMM yyyy").format(hijrahDate);
    }

    private LocalTime getTime(Row row, int col) {
        return row.getCell(col).getDateCellValue().toInstant().atZone(ZoneId.systemDefault()).toLocalTime();
    }
}
