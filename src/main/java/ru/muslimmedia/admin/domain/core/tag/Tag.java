package ru.muslimmedia.admin.domain.core.tag;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import ru.muslimmedia.admin.domain.core.BaseEntity;

import javax.persistence.Entity;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Tag extends BaseEntity {

    @NotEmpty
    private String title;

    private Integer weight;

    public Tag(String title) {
        this.title = title;
    }
}
