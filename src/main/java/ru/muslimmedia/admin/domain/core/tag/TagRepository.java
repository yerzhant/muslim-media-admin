package ru.muslimmedia.admin.domain.core.tag;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

@SuppressWarnings("unused")
public interface TagRepository extends CrudRepository<Tag, Long> {

    Tag findByTitle(String title);

    @Query(value =  "select id, title, count(*) weight from tag t" +
                    "  left join video_tags v on t.id = v.tags_id" +
                    "  left join post_tags p on t.id = p.tags_id" +
                    " where video_id is not null or post_id is not null" +
                    " group by id, title limit 20",
            nativeQuery = true)
    List<Tag> getTagWeights();
}
