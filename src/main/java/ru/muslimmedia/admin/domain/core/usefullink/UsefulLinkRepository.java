package ru.muslimmedia.admin.domain.core.usefullink;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

@SuppressWarnings("unused")
public interface UsefulLinkRepository extends CrudRepository<UsefulLink, Long> {

    List<UsefulLink> findByOrderByWeight();
}
