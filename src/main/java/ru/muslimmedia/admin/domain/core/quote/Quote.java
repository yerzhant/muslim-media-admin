package ru.muslimmedia.admin.domain.core.quote;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import ru.muslimmedia.admin.domain.core.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Entity
@Data
class Quote extends BaseEntity {

    @SuppressWarnings("unused")
    private enum Type {
        GOLD, SILVER, FITR, FIDYA, MAHR, NISAB
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    private Type type;

    @NotEmpty
    private String price;

    private Integer weight;
}
