package ru.muslimmedia.admin.domain.core.calendar;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import ru.muslimmedia.admin.domain.core.BaseEntity;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Data
class Calendar extends BaseEntity {

    @NotEmpty
    private String title;

    @NotNull
    private LocalDate date;
}
