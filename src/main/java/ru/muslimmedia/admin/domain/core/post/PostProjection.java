package ru.muslimmedia.admin.domain.core.post;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import ru.muslimmedia.admin.domain.core.section.Section;

import java.time.LocalDate;

@SuppressWarnings("unused")
@Projection(types = Post.class)
public interface PostProjection {

    @Value("#{target.id}")
    Long getId();

    String getTitle();

    String getText();

    @JsonFormat(pattern = "d MMMM yyyy")
    LocalDate getPublishedOn();

    Integer getViews();

    Section getSection();
}
