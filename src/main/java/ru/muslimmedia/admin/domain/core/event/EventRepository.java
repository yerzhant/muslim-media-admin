package ru.muslimmedia.admin.domain.core.event;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

@SuppressWarnings("unused")
public interface EventRepository extends CrudRepository<Event, Long> {

    Event findTop1ByOrderByPublishedOnDesc();

    Page findByOrderByPublishedOnDesc(Pageable pageable);
}
