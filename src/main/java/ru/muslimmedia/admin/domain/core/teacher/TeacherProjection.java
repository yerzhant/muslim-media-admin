package ru.muslimmedia.admin.domain.core.teacher;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@SuppressWarnings("unused")
@Projection(types = Teacher.class)
public interface TeacherProjection {

    @Value("#{target.id}")
    Long getId();

    String getName();

    String getDescription();

    String getImage();
}
