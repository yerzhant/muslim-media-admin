package ru.muslimmedia.admin.domain.core.setting;

import org.springframework.data.repository.CrudRepository;

public interface SettingRepository extends CrudRepository<Setting, Long> {

    Setting findByType(Setting.Type type);
}
