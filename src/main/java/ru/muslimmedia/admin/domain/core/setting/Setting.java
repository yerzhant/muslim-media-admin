package ru.muslimmedia.admin.domain.core.setting;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import ru.muslimmedia.admin.domain.core.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Setting extends BaseEntity {

    public enum Type {
        HIJRAH_CORRECTION, YOUTUBE, FACEBOOK, TWITTER, VK
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    private Type type;

    @NotEmpty
    private String value;
}
