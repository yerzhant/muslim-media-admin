package ru.muslimmedia.admin.domain.core.prayer;

import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.List;

@SuppressWarnings("unused")
public interface PrayerRepository extends CrudRepository<Prayer, Long> {

    Prayer findByDateAndCity(LocalDate date, String city);

    List<City> findDistinctCityByOrderByCity();

    interface City {
        String getCity();
    }
}
