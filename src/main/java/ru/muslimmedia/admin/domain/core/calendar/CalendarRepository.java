package ru.muslimmedia.admin.domain.core.calendar;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

@SuppressWarnings("unused")
public interface CalendarRepository extends CrudRepository<Calendar, Long> {

    List<Calendar> findByOrderByDate();
}
