package ru.muslimmedia.admin.domain.core.section;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@SuppressWarnings("unused")
@Projection(types = Section.class)
public interface SectionWithId {

    @Value("#{target.id}")
    Long getId();

    String getTitle();
}
