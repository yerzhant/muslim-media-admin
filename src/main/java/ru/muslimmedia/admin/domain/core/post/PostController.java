package ru.muslimmedia.admin.domain.core.post;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@BasePathAwareController
@RequestMapping("posts")
public class PostController {

    @Autowired
    private PostService service;

    @PatchMapping("increase-views/{id}")
    public void increaseViews(@PathVariable Long id) {
        service.increaseViews(id);
    }
}
