package ru.muslimmedia.admin.domain.core.event;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import ru.muslimmedia.admin.domain.core.BaseEntity;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Data
public class Event extends BaseEntity {

    @NotEmpty
    private String text;

    @NotNull
    private LocalDate publishedOn;
}
