package ru.muslimmedia.admin.domain.core.video;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import ru.muslimmedia.admin.domain.core.BaseEntity;
import ru.muslimmedia.admin.domain.core.section.Section;
import ru.muslimmedia.admin.domain.core.tag.Tag;
import ru.muslimmedia.admin.domain.core.teacher.Teacher;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Data
public class Video extends BaseEntity {

    @NotEmpty
    private String title;

    @NotEmpty
    private String videoId;

    @NotNull
    private LocalDate publishedOn;

    @NotNull
    @Min(0)
    private Integer views;

    private Boolean showInSlider;

    private String image;

    @ManyToOne
    private Teacher teacher;

    @ManyToMany
    private Set<Tag> tags;

    @NotNull
    @ManyToOne
    private Section section;

    public int increaseViews() {
        return ++views;
    }
}
