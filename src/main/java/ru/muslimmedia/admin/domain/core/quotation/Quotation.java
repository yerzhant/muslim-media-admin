package ru.muslimmedia.admin.domain.core.quotation;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import ru.muslimmedia.admin.domain.core.BaseEntity;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Data
public class Quotation extends BaseEntity {

    @NotEmpty
    private String text;

    private String author;

    @NotNull
    private LocalDate publishedOn;
}
