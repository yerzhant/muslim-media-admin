package ru.muslimmedia.admin.domain.core.post;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import ru.muslimmedia.admin.domain.core.BaseEntity;
import ru.muslimmedia.admin.domain.core.section.Section;
import ru.muslimmedia.admin.domain.core.tag.Tag;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Data
public class Post extends BaseEntity {

    @NotEmpty
    private String title;

    @NotEmpty
    @Lob
    private String text;

    @NotNull
    private LocalDate publishedOn;

    @NotNull
    @Min(0)
    private Integer views;

    @ManyToMany
    private Set<Tag> tags;

    @NotNull
    @ManyToOne
    private Section section;

    public void increaseViews() {
        views++;
    }
}
