package ru.muslimmedia.admin.domain.core.book;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import ru.muslimmedia.admin.domain.core.section.Section;

@SuppressWarnings("unused")
public interface BookRepository extends CrudRepository<Book, Long> {

    Page findBySectionOrderByPublishedOnDesc(Section section, Pageable pageable);

    Page findByTitleContainsIgnoreCaseOrderByPublishedOnDesc(String title, Pageable pageable);
}
