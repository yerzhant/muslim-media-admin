package ru.muslimmedia.admin.domain.core.prayer;

import lombok.AllArgsConstructor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@BasePathAwareController
@RequestMapping("prayer-times")
public class PrayerController {

    private final PrayerService service;

    @Autowired
    public PrayerController(PrayerService service) {
        this.service = service;
    }

    @PostMapping("load")
    public void loadFile(@RequestParam("file") MultipartFile file) throws IOException, InvalidFormatException {
        service.loadFile(file);
    }

    @GetMapping("cities")
    public List<String> cities() {
        return service.getAllCities();
    }

    @GetMapping("today/{city}")
    public PrayerDto today(@PathVariable String city) {
        Prayer prayer = service.getPrayerTimesForToday(city);
        String hijrahDate = service.getHijrahDate();
        return new PrayerDto(prayer, hijrahDate);
    }

    @AllArgsConstructor
    private static final class PrayerDto {
        public Prayer prayer;
        public String hijrahDate;
    }
}
