package ru.muslimmedia.admin.domain.core.partner;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import ru.muslimmedia.admin.domain.core.BaseEntity;

import javax.persistence.Entity;

@Entity
@Data
class Partner extends BaseEntity {

    @NotEmpty
    private String title;

    @NotEmpty
    private String url;

    private Integer weight;
}
