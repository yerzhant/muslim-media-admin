package ru.muslimmedia.admin.domain.core.video;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import ru.muslimmedia.admin.domain.core.section.Section;

import java.util.List;

@SuppressWarnings("unused")
public interface VideoRepository extends CrudRepository<Video, Long> {

    List findTop6ByOrderByPublishedOnDesc();

    List findTop6ByOrderByViewsDesc();

    List findTop6ByShowInSliderTrueOrderByPublishedOnDesc();

    Page findBySectionOrderByPublishedOnDesc(Section section, Pageable pageable);

    Page findDistinctByTitleContainsOrTagsTitleContainsAllIgnoreCaseOrderByPublishedOnDesc(String title, String tagTitle, Pageable pageable);

    Page findDistinctByTagsTitleContainsAllIgnoreCaseOrderByPublishedOnDesc(String tagTitle, Pageable pageable);
}
