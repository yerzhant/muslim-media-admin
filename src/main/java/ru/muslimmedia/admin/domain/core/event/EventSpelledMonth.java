package ru.muslimmedia.admin.domain.core.event;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.data.rest.core.config.Projection;

import java.time.LocalDate;

@SuppressWarnings("unused")
@Projection(types = Event.class)
public interface EventSpelledMonth {

    String getText();

    @JsonFormat(pattern = "d MMMM yyyy")
    LocalDate getPublishedOn();
}
