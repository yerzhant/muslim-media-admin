package ru.muslimmedia.admin.domain.core.video;

public interface VideoService {

    int increaseViews(String uri);
}
