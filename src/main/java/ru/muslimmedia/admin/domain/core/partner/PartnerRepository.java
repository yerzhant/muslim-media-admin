package ru.muslimmedia.admin.domain.core.partner;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

@SuppressWarnings("unused")
public interface PartnerRepository extends CrudRepository<Partner, Long> {

    List<Partner> findByOrderByWeight();
}
