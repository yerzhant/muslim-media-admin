package ru.muslimmedia.admin.domain.core.video;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@BasePathAwareController
@RequestMapping("videos")
public class VideoController {

    @Autowired
    private VideoService service;

    @PatchMapping("increase-views")
    public int increaseViews(@RequestBody Uri uri) {
        return service.increaseViews(uri.uri);
    }

    @Data
    private static class Uri {
        private String uri;
    }
}
