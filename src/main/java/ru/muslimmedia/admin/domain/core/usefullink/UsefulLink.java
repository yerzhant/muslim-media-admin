package ru.muslimmedia.admin.domain.core.usefullink;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import ru.muslimmedia.admin.domain.core.BaseEntity;

import javax.persistence.Entity;

@Entity
@Data
class UsefulLink extends BaseEntity {

    @NotEmpty
    private String title;

    @NotEmpty
    private String url;

    private Integer weight;
}
