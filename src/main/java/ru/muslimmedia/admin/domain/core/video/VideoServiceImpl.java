package ru.muslimmedia.admin.domain.core.video;

import org.springframework.stereotype.Service;
import ru.muslimmedia.admin.domain.util.Utils;

@Service
public class VideoServiceImpl implements VideoService {

    private final VideoRepository repository;

    public VideoServiceImpl(VideoRepository repository) {
        this.repository = repository;
    }

    @Override
    public int increaseViews(String uri) {
        Video video = repository.findOne(Utils.getIdFromUri(uri, "videos"));
        int views = video.increaseViews();
        repository.save(video);
        return views;
    }
}
