package ru.muslimmedia.admin.domain.core.calendar;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.data.rest.core.config.Projection;

import java.time.LocalDate;

@SuppressWarnings("unused")
@Projection(types = Calendar.class)
public interface CalendarSpelledMonth {

    String getTitle();

    @JsonFormat(pattern = "d MMMM yyyy")
    LocalDate getDate();
}
