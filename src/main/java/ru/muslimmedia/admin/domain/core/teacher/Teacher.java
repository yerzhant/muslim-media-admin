package ru.muslimmedia.admin.domain.core.teacher;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import ru.muslimmedia.admin.domain.core.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Lob;

@Entity
@Data
public class Teacher extends BaseEntity {

    @NotEmpty
    private String name;

    @NotEmpty
    @Lob
    private String description;

    @NotEmpty
    private String image;

    private Integer weight;
}
