package ru.muslimmedia.admin.domain.core.post;

import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService {

    private final PostRepository repository;

    public PostServiceImpl(PostRepository repository) {
        this.repository = repository;
    }

    @Override
    public void increaseViews(Long id) {
        Post post = repository.findOne(id);
        post.increaseViews();
        repository.save(post);
    }
}
