package ru.muslimmedia.admin.domain.core.prayer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import ru.muslimmedia.admin.domain.core.BaseEntity;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Prayer extends BaseEntity {

    @NotEmpty
    private String city;

    @NotNull
    private LocalDate date;

    @NotNull
    private LocalTime fajr;

    @NotNull
    private LocalTime sunrise;

    @NotNull
    private LocalTime dhuhr;

    @NotNull
    private LocalTime asr;

    @NotNull
    private LocalTime maghrib;

    @NotNull
    private LocalTime isha;
}
