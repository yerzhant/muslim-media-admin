package ru.muslimmedia.admin.domain.core.prayer;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface PrayerService {

    void loadFile(MultipartFile file) throws IOException, InvalidFormatException;

    Prayer getPrayerTimesForToday(String city);

    List<String> getAllCities();

    String getHijrahDate();
}
