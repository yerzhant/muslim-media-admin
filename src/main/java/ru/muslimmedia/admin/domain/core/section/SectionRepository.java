package ru.muslimmedia.admin.domain.core.section;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

@SuppressWarnings("unused")
public interface SectionRepository extends CrudRepository<Section, Long> {

    Page findByType(Section.Type type, Pageable pageable);
}
