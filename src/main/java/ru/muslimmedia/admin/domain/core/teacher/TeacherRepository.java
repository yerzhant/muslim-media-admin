package ru.muslimmedia.admin.domain.core.teacher;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

@SuppressWarnings("unused")
public interface TeacherRepository extends CrudRepository<Teacher, Long> {

    List<Teacher> findByOrderByWeightDesc();
}
