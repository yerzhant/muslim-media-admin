package ru.muslimmedia.admin.domain.core.video;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import ru.muslimmedia.admin.domain.core.section.Section;
import ru.muslimmedia.admin.domain.core.tag.Tag;
import ru.muslimmedia.admin.domain.core.teacher.Teacher;

import java.time.LocalDate;
import java.util.Set;

@SuppressWarnings("unused")
@Projection(types = Video.class)
interface VideoProjection {

    @Value("#{target.id}")
    Long getId();

    String getTitle();

    String getVideoId();

    @JsonFormat(pattern = "d MMMM yyyy")
    LocalDate getPublishedOn();

    Integer getViews();

    Boolean getShowInSlider();

    String getImage();

    Teacher getTeacher();

    Set<Tag> getTags();

    Section getSection();
}
