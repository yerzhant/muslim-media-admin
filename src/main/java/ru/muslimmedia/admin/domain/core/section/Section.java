package ru.muslimmedia.admin.domain.core.section;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import ru.muslimmedia.admin.domain.core.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Entity
@Data
public class Section extends BaseEntity {

    @SuppressWarnings("unused")
    public enum Type {
        MADRASA, TEXTS, LIBRARY
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    private Type type;

    @NotEmpty
    private String title;

    @NotNull
    private Integer weight;
}
