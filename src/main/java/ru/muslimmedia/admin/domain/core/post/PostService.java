package ru.muslimmedia.admin.domain.core.post;

public interface PostService {

    void increaseViews(Long id);
}
