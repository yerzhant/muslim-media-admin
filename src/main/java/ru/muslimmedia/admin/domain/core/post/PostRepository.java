package ru.muslimmedia.admin.domain.core.post;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import ru.muslimmedia.admin.domain.core.section.Section;

@SuppressWarnings("unused")
public interface PostRepository extends CrudRepository<Post, Long> {

    Page findBySectionOrderByPublishedOnDesc(Section section, Pageable pageable);

    Page findDistinctByTitleContainsOrTextContainsOrTagsTitleContainsAllIgnoreCaseOrderByPublishedOnDesc(String title, String text, String tagTitle, Pageable pageable);

    Page findDistinctByTagsTitleContainsAllIgnoreCaseOrderByPublishedOnDesc(String tagTitle, Pageable pageable);
}
